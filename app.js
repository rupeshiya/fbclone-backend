const express = require('express');
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
const expressValidator = require('express-validator');
const fs = require('fs');
const cors = require('cors');
const dotenv = require('dotenv');
const passport = require('passport');
const compression = require("compression");
dotenv.config();

// db connection 
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(()=>{
    console.log("Mongo connected successfully!!");
  })
  .catch((err)=>{
    console.log("Error in connecting mongo!!", err);
  });

mongoose.connection.on('error', err => {
  console.log(`DB connection error: ${err.message}`);
});

// bring in routes
const postRoutes = require('./routes/post');
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/users');
// apiDocs
app.get('/api', (req, res) => {
  fs.readFile('docs/apiDocs.json', (err, data) => {
    if (err) {
      res.status(400).json({
        error: err
      });
    }
    const docs = JSON.parse(data);
    res.json(docs);
  });
});

// middleware -
app.use(morgan('tiny'));
// passport middleware
app.use(passport.initialize());
// passport config
require('./config/passport')(passport);
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressValidator());
app.use(cors());
app.use(compression());

app.get('/test', (req, res)=>{
  res.status(200).json({success: true, msg: '/test'});
})
app.use('/api', postRoutes);
app.use('/api', authRoutes);
app.use('/api', userRoutes);
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).json({
      error: 'Unauthorized!'
    });
  }
  if(err.name === 'MongoNetworkError'){
    res.status(400).json({
      error: "Mongo connection error!"
    });
  }
  next();
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`listening on port: ${port}`);
});
