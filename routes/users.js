const express = require('express');
const router = express.Router();

const { 
  allUser,
  getUserById,
  updateUser,
  deleteUser,
  addFollowing,
  addFollowers,
  removeFollowing,
  removeFromFollowersList,
  findPeople
} = require('../controllers/user.controller');

const passport = require('passport');

// get all the users 
router.get('/users/all',passport.authenticate('jwt', { session: false }), allUser);

// get single user by id 
router.get('/users/:userId',passport.authenticate('jwt', { session: false }), getUserById);

// update the user 
router.put('/users/update/:userId', passport.authenticate('jwt', { session: false }), updateUser)

// delete the user 
router.delete('/users/delete/:userId', passport.authenticate('jwt', { session: false }), deleteUser);

// follow the user (send req.body.followId from client side)
router.post('/users/follow', passport.authenticate('jwt', {session: false}), addFollowing, addFollowers);

// unfollow the user 
router.post('/users/unfollow', passport.authenticate('jwt', {session: false}), removeFollowing, removeFromFollowersList);

// explore the friends 
router.get('/users/find/friends', passport.authenticate('jwt', { session: false }), findPeople);

module.exports = router;