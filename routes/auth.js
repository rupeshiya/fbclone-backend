const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const User = require('../models/User');
const authController = require('../controllers/auth.controller');

// register locally 
router.post('/auth/register', authController.registerUser);

// locally login 
router.post('/auth/authenticate', authController.loginUser);

module.exports = router;