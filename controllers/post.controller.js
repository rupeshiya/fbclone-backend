const mongoose = require('mongoose');
const Post = require('../models/Post');
const formidable = require('formidable');
const fs = require('fs');
const User = require('../models/User');

exports.createPost = (req, res) => {
  //create the post
  let form = new formidable.IncomingForm();
  form.keepExtensions = true;
  form.parse(req, (err, fields, files)=>{
    if(err){
      console.log(err);
      return res.status(400).json({success: false, err });
    } else {
      console.log("fields ", fields);
      const newPost = new Post(fields);
      newPost.postedBy = req.user._id;
      if(files.photo){
        newPost.photo.data = fs.readFileSync(files.photo.path);
        newPost.photo.contentType = files.photo.type;
      }
      newPost
        .save()
        .then(post => {
          console.log("Post saved!", post);
          res
            .status(201)
            .json({ success: true, msg: "Post created!", post: post });
        })
        .catch(err => {
          console.log("Error ", err);
          return res.status(400).json({ success: false, err: err });
        });
    }
  })
}

// get all the posts
exports.getPosts = (req, res) => {
  Post.find({})
  .select('_id title body')
  .populate('postedBy', ['_id', 'name', 'email'])
    .exec((err, posts)=>{
      if(err){
        console.log(err);
        return res.status(400).json({success: false, err: err });
      } else {
        res.status(200).json({success: true, posts: posts });
      }
    });
}

// to get all the posts according to the following list of a user 
exports.getPostFeed = (req, res) => {
  User.findById(req.user._id)
  .populate('followings', ['_id'])
  .exec((err, user)=>{
    if(err){
      console.log(err);
      return res.status(400).json({success: false, err });
    }
    user.followings.push(req.user._id); // to fetch all the post related to the user itself and his following 
    Post.find({ _id : { $in: user.followings }})
      .populate('postedBy', ['_id', 'name', 'username'])
      .populate('comments', ['text', 'created'])
      .populate('comments.postedBy', ['_id', 'name', 'username'])
      .select('_id title body photo comments likes created')
      .exec((err, posts)=>{
        if(err || !posts){
          return res.status(400).json({success: false, msg: 'Error in fetching posts!'});
        }
        return res.status(200).json({success: false, posts: posts });
      });
  })
}

// get post by id 
exports.getPostById = (req, res) => {
  Post.findById(req.prams.postId)
    .populate('postedBy', ['name', '_id', 'username'])
    .populate('comments', ['created', 'text'])
    .populate('comments.postedBy', ['_id', 'name', 'username'])
    .select('_id title body photo comments likes created')
    .exec((err, result)=>{
      if(err){
        console.log(err);
        return res.status(400).json({success: false, err});
      } else {
        console.log(result);
        return res.status(200).json({ success: true, post: result });
      }
    });
}


// get all post using pagination 
exports.getAllPosts = (req, res) => {
  // /api/posts/all?page=3
  let nextPage = req.query.page || 1;
  let totalDocs; 
  let perPageDocs = 5;

  Post.find({})
    .countDocuments()
    .then((count)=>{
     // count total documents 
      totalDocs = count;
      return Post.find({})
        .skip((nextPage - 1) * perPageDocs)
        .populate('comments', ['text', 'created'])
        .populate('postedBy', ['_id', 'name', 'username'])
        .populate('comments.postedBy', ['_id', 'name', 'username'])
        .select('_id title body created likes')
        .limit(perPageDocs)
        .sort({created: -1})
    })
    .then((posts)=>{
        return res.status(200).json({success: true, posts: posts });
    })
    .catch((err)=>{
      console.log(err);
      return res.status(400).json({success: false, err });
    });
}

// get all the post of a particular users 
exports.getAllPostOfAUser = (req, res) => {
  Post.find({postedBy: req.user._id})
    .populate('comments', ['_id', 'created', 'text'])
    .populate('postedBy', ['_id', 'name', 'username'])
    .populate('comments.postedBy', ['_id', 'name', 'username'])
    .select('_id title body photo comments likes created postedBy')
    .sort({created: -1 })
    .exec((err, posts)=>{
      if(err){
        console.log(err);
        return res.status(400).json({success: false, err});
      } else {
        return res.status(200).json({success: true, posts: posts});
      }
    });
}

// delete the post 
exports.deletePostById = (req, res) => {
  Post.findByIdAndRemove(req.params.postId)
    .then((post)=>{
      console.loginField('Deleted successfully!');
      return res.status(200).json({success: true, msg: 'successfully deleted!'});
    })
    .catch((err)=>{
      console.log(err);
      return res.status(400).json({success: false, err});
    });
}

// like the post 
exports.likePost = (req, res) => {
  Post.findById(req.params.postId)
    .then((post)=>{
      if(!post){
        return res.status(404).json({success: false, msg: 'Post does not exists!'});
      } else {
        // check if already liked => likes array will have userId
        const likesIdArray = post.likes.map(like => like._id);
        const userIdArray = likesIdArray.filter(likeId => likeId.toString() == req.user._id.toString());
        if(userIdArray){
          console.log("Already liked!!");
          return res.status(400).json({success: true, msg: 'Already liked the post!'});
        } else {
          // push the userId into the like array 
          post.likes.unshift(req.user._id);
          post.save()
          .then((result)=>{
            return res.status(201).json({success: true, msg: 'Liked the post!'});
          })
          .catch((err)=>{
            console.log(err);
            return res.status(400).json({success: false, err });
          });
        }
      }
    })
    .catch((err)=>{
      console.log(err);
      return res.status(400).json({success: false, err });
    });
}

// unlike the post 
exports.unlikePost = (req, res) => {
  Post.findById(req.params.postId)
    .then((post)=>{
      if(!post){
        return res.status(404).json({success: false, msg: 'Post does not exists!'});
      } else {
        const likesIdArray = post.likes.map(like => like._id);
        const indexOfUserLikeId = likesIdArray.indexOf(req.user._id);
        if(indexOfUserLikeId){
          post.likes.splice(indexOfUserLikeId, 1);
          post.save()
          .then((results)=>{
            console.log(results);
            return res.status(200).json({success: true, msg: 'successfully unliked the post'})
          })
          .catch((err)=>{
            console.log(err);
            return res.status(404).json({success: false, err });
          })
        }
      }
    })
    .catch((err)=>{
      console.log(err);
      return res.status(400).json({success: false, err});
    });
}

// add comment
exports.addComment = (req, res) => {
  Post.findById(req.params.postId)
    .then((post)=>{
      if(!post){
        return res.status(404).json({success: false, msg: 'Post not found!'});
      } else {
        const comment = {
          text: req.body.commentText,
          created: Date.now(),
          postedBy: req.user._id
        };
        post.comments.unshift(comment)
        post.save()
        .then((result) => {
          console.log(result);
          return res.status(200).json({success: true, msg: 'successfully added the comment!'})
        })
        .catch((err)=>{
          console.log(err);
          return res.status(400).json({success: false, err});
        });
      }
    })
    .catch((err)=>{
      console.log(err);
      return res.status(400).json({success: false, err });
    });
}

// delete comment
exports.deleteComment = (req, res) => {
  Post.findById(req.params.postId)
    .then((post)=>{
      if(!post){
        return res.status(404).json({success: false, msg: "post not found!"});
      } else {
        // check if commented or not 
        const commenterIdArray = post.comments.map(comment => comment.postedBy);
        const userCommentId = commenterIdArray.indexOf(req.user._id);
        if(!userCommentId){
          return res.status(400).json({success: false, msg: 'You have not made any comment yet on this post!'});
        } else {
          // remove the comment 
          post.comments.splice(userCommentId, 1);
          post.save()
          .then((result)=>{
            console.log(result);
            return res.status(200).json({success: true ,msg: 'successfully deleted the comment!'});
          });
        }
      }
    })
    .catch((err)=>{
      console.log(err);
      return res.status(404).json({success: false, err });
    });
}