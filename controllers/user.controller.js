const mongoose = require('mongoose');
const Users = require('../models/User');

// get all the users 
exports.allUser = (req, res )=>{
  Users.find({})
  .populate('followers', ['_id', 'name', 'username', 'email'])
  .populate('followings', ['_id', 'name', 'username', 'email'])
  .lean()
  .select('_id name email username')
    .exec((err,users)=>{
      if(err){
        console.log(err);
        return res.status(404).json({success: false, msg: 'Users not found!!'});
      }
      if(users){
        return res.status(200).json({success: true, users: users });
      }
    });
}

// get single user
exports.getUserById = (req, res) => {
  Users.findById(req.params.userId)
  .populate('followings', ['_id', 'name', 'email', 'username'])
  .populate('followers', ['_id', 'name', 'email', 'username'])
  .select('_id name email username created updated')
    .exec((err, user)=>{
      if(err){
         console.log(err);
        return res.status(400).json({success: false, err});
      }
      if(!user){
        return res.status(404).json({success: false, msg: `User with ${req.params.userId} not found!`});
      } else {
        return res.status(200).json({success: true, msg: 'User found!', user: user});
      }
    })
}

// update the user
exports.updateUser = (req, res) => {
  const { name , email, username } = req.body;
  // check if user exists 
  Users.findById(req.params.userId)
    .then((user)=>{
      if(!user){
        return res.status(404).json({success: false, msg: 'User does not exists!'});
      } else {
        user.name = name;
        user.email = email;
        user.username = username;
        user.updated = Date.now();
        user.save()
          .then((result)=>{
            console.log("User updated successfully!");
            return res.status(201).json({success: true, msg: 'Updated successfully!'});
          })
          .catch((err)=>{
            console.log(err);
            return res.status(400).json({success: false, err });
          });
      }
    })
    .catch((err)=>{
      console.log(err);
      return res.status(400).json({success: false, err});
    });
}

// delete user 
exports.deleteUser = (req, res) => {
  Users.findByIdAndRemove(req.params.userId)
    .then((user)=>{
      return res.status(200).json({success: true, msg: 'successfully deleted the user!'});
    })
    .catch((err)=>{
      console.log(err);
      return res.status(400).json({success: false, err: err });
    });
}

 // add followers
 exports.addFollowing = (req, res, next) => {
   Users.findById(req.user._id)
    .then((user)=>{
      console.log(user);
      user.followings.unshift(req.body.followId);
      user.save()
        .then((result)=>{
          console.log(result);
          next();
        })
    })
    .catch((err)=>{
      console.log(err);
      return res.status(404).json({success: false, err });
    });
 } 
 
 // add to the following 
 exports.addFollowers = (req, res) => {
  Users.findById(req.body.followId)
    .populate('following', '_id name')
    .populate('followers', '_id name')
    .exec((err, user)=>{
      if(err){
        console.log(err);
        return res.status(400).json({success: false, err });
      } else {
        user.followers.unshift(req.user._id);
        user.save()
        .then((results)=>{
          console.log(results);
          return res.status(200).json({success: true , msg: 'successfully added to the followers list!'});
        })
        .catch((err)=>{
          console.log(err);
          return res.status(400).json({success: false, err });
        });
      }
    })
 }

 // remove following/unfollow 
exports.removeFollowing = (req, res, next) => {
  Users.findById(req.user._id)
    .then((user)=>{
      // check if followId is in following list or not 
      const followingIdArray = user.followings.map(followingId => followingId._id);
      const unFollowIndexArray = user.followings.filter((following) => {
        return following._id.toString() === req.body.followId;
      });
      const isFollowingIdIndex = followingIdArray.indexOf(unFollowIndexArray[0]);
      if (isFollowingIdIndex == null || isFollowingIdIndex == undefined) {
        console.log("Not following!");
        return res.status(400).json({success: false, msg: 'You have not followed the user!'});
      } else {
        // if in the following list remove from following list 
        user.followings.splice(isFollowingIdIndex, 1);
        user.save()
        .then((result)=>{
          console.log(result);
          next();
        })
        .catch((err)=>{
          console.log(err);
          return res.status(400).json({success: false, err });
        });
      }
    })
}

 // remove from followers list 
 exports.removeFromFollowersList = (req, res)=>{
   Users.findById(req.body.followId)
    .populate('following', '_id name')
    .populate('followers', '_id name')
    .exec((err, user)=>{
      if(err){
        console.log(err);
        return res.status(400).json({success: false, err });
      } else {
          console.log(user);
          const followersIdArray = user.followers.map(follower => follower._id);
          const unFollowIndexArray = user.followings.filter((follower) => {
            return follower._id.toString() === req.user._id;
          });
          const isFollowing = followersIdArray.indexOf(unFollowIndexArray[0]);
          if(isFollowing == null || isFollowing == undefined){
            return res.status(400).json({success: false, msg: 'User is not following!'});
          } else {
            // if following then on unFollow remove from followers list 
            user.followers.splice(isFollowing, 1);
            user.save()
              .then((results)=>{
                console.log(results);
                return res.status(200).json({success: true, msg: 'Removed from followers list!'});
              });
          }
      }
    })
 }

// find people 
exports.findPeople = (req, res) => {
  let followings;
  Users.findById(req.user._id)
    .exec((err, user)=>{
      if(err){
        console.log(err);
        return res.status(400).json({success: false, err });
      } else {
        followings = user.followings;
        followings.push(req.user._id); // pushing for the purpose to find people except user itself
        Users.find({_id: { $nin: followings } }, (err, results)=>{
          if(err){
            console.log(err);
            return res.status(400).json({success: false, err});
          } else {
            return res.status(200).json({success: true, results});
          }
        }) // find all the user who is not in the following 
        .populate('followings', '_id name')
        .populate('followers', '_id name')
        .select('_id name username email created');
      }
    });
}